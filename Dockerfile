# syntax=docker/dockerfile:1
FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    python3-dev \
    python3-pip\
    libpq-dev \
    postgresql \
    postgresql-contrib \
    curl \
    vim \
    npm

ADD server /opt/server
ADD client /opt/client

RUN /bin/bash -c 'cd /opt/server && pip install -r requirements.txt'

ENV PYTHONPATH=.:./apps
ENV DJANGO_SETTINGS_MODULE=settings.dev

WORKDIR /app

EXPOSE 8000 4200

ADD entry.sh /opt/

RUN chmod 755 /opt/entry.sh

ENTRYPOINT [ "/opt/entry.sh" ]