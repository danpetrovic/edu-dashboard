DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'educational-dashboard',
        'USER': 'dan',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

SECRET_KEY = "tempsecretkey"
