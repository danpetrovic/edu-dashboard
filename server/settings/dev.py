from django.core.exceptions import ImproperlyConfigured

from settings.base import *  # noqa


DEBUG = True
AUTH_PASSWORD_VALIDATORS = []
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
ALLOWED_HOSTS = ["localhost"]
CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOW_HEADERS = "*"

try:
    from settings.local import *
except ImportError:
    raise ImproperlyConfigured("Cannot find local settings file for development.")

