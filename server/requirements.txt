-i https://pypi.org/simple
asgiref==3.7.2; python_version >= '3.7'
django==4.2.8
django-cors-headers==4.3.1
sqlparse==0.4.4; python_version >= '3.5'
djangorestframework==3.14.0
pillow==10.1.0
psycopg2==2.9.9
pytz==2023.3.post1
