from django.db import models

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from core.models import (
    Subject,
    Teacher,
    ActivityAssessment,
    SubjectClass,
    StudentProgress,
    ResourceManagement,
    Coach,
    CoachTeacherInteraction
)
from core.serializers import (
    SubjectSerializer,
    TeacherSerializer,
    ActivityAssessmentSerializer,
    SubjectClassSerializer,
    StudentProgressSerializer,
    ResourceManagementSerializer,
    CoachSerializer,
    CoachTeacherInteractionSerializer
)


class OverviewData(APIView):
    def get(self, request, *args, **kwargs):
        teacher_activities = list()
        for teacher in Teacher.objects.all().prefetch_related(
                models.Prefetch("assessments", ActivityAssessment.objects.all())
        ):
            most_recent_assessment = teacher.assessments.last()
            teacher_activities.append({
                "teacher_id": teacher.id,
                "teacher_avatar": teacher.avatar.url if teacher.avatar else None,
                "name": teacher.name,
                "last_active": most_recent_assessment.date.strftime("%Y-%m-%d") if most_recent_assessment else None,
                "activity_score": most_recent_assessment.score if most_recent_assessment else None,
                "student_interaction_rating": most_recent_assessment.student_interaction_rating if most_recent_assessment else None,
                "subjects_taught": set(SubjectClass.objects.filter(teacher=teacher).values_list("subject__name", flat=True))
            })
        student_progress = list()
        for subject_class in SubjectClass.objects.all().prefetch_related(
            models.Prefetch("student_progress", StudentProgress.objects.all())
        ):
            improvement = None
            if subject_class.student_progress.count() > 1:
                last = subject_class.student_progress.all()[0]
                second_last = subject_class.student_progress.all()[1]
                improvement = last.average_score - second_last.average_score
            if improvement is not None:
                student_progress.append({
                    "class_id": subject_class.id,
                    "subject": subject_class.subject.name,
                    "average_score_improvement": improvement,
                    "homework_completion_rate": last.homework_completion_rate,
                    "attendance_rate": last.attendance_rate
                })
        resource_management = list()
        for resource in ResourceManagement.objects.all():
            resource_management.append({
                "resource_id": resource.id,
                "resource_name": resource.name,
                "allocated_teachers": resource.allocated_teachers.values("id", "name"),
                "utilization_rate": resource.utilization_rate
            })
        coach_details = list()
        for coach in Coach.objects.all():
            coach_details.append({
                "coach_id": coach.id,
                "name": coach.name,
                "specialization": coach.specialization,
                "years_of_experience": coach.years_of_experience
            })
        coach_teacher_interactions = list()
        for teacher in Teacher.objects.all():
            interaction = CoachTeacherInteraction.objects.filter(teacher=teacher).order_by("-meeting_date").last()
            if interaction:
                coach_teacher_interactions.append({
                    "coach": interaction.coach.name,
                    "teacher": interaction.teacher.name,
                    "last_meeting_date": interaction.meeting_date.strftime("%Y-%m-%d"),
                    "meeting_notes": interaction.meeting_notes
                })
        return Response(
            status=200, data={
                "teacher_activities": teacher_activities,
                "student_progress": student_progress,
                "resource_management": resource_management,
                "coach_details": coach_details,
                "coach_teacher_interactions": sorted(coach_teacher_interactions, key=lambda v: v["last_meeting_date"], reverse=True)
            }
        )


class SubjectViewset(viewsets.ModelViewSet):
    serializer_class = SubjectSerializer
    queryset = Subject.objects.all()


class TeacherViewset(viewsets.ModelViewSet):
    parser_classes = [MultiPartParser]
    serializer_class = TeacherSerializer

    def get_queryset(self):
        qs = Teacher.objects.all()
        q = self.request.GET.get("q", None)
        if q:
            qs = qs.filter(name__icontains=q)
        return qs

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["action"] = self.action
        return context


class ActivityAssessmentViewset(viewsets.ModelViewSet):
    serializer_class = ActivityAssessmentSerializer

    def get_queryset(self):
        qs = ActivityAssessment.objects.all()
        teacher_id = self.request.GET.get("t", None)
        if teacher_id is not None:
            qs = qs.filter(teacher=teacher_id)
        return qs

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["action"] = self.action
        return context


class SubjectClassViewset(viewsets.ModelViewSet):
    serializer_class = SubjectClassSerializer
    queryset = SubjectClass.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["action"] = self.action
        return context


class StudentProgressViewset(viewsets.ModelViewSet):
    serializer_class = StudentProgressSerializer

    def get_queryset(self):
        qs = StudentProgress.objects.all()
        subject = self.request.GET.get("subject", None)
        teacher = self.request.GET.get("teacher", None)
        if subject:
            qs = qs.filter(subject_class__subject=subject)
        if teacher:
            qs = qs.filter(subject_class__teacher=teacher)
        return qs


class ResourceManagementViewset(viewsets.ModelViewSet):
    serializer_class = ResourceManagementSerializer
    queryset = ResourceManagement.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["action"] = self.action
        return context


class CoachViewset(viewsets.ModelViewSet):
    parser_classes = [MultiPartParser]
    serializer_class = CoachSerializer
    queryset = Coach.objects.all()


class CoachTeacherInteractionViewset(viewsets.ModelViewSet):
    serializer_class = CoachTeacherInteractionSerializer
    queryset = CoachTeacherInteraction.objects.all()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["action"] = self.action
        return context
