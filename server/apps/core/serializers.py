from rest_framework import serializers


from core.models import (
    Subject,
    Teacher,
    ActivityAssessment,
    SubjectClass,
    StudentProgress,
    ResourceManagement,
    Coach,
    CoachTeacherInteraction
)


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = "__all__"
        read_only_fields = ("id",)


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = "__all__"
        read_only_fields = ("id",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if self.context["action"] == "list":
            ret["classes"] = instance.classes.values_list("subject__name", flat=True)
        if self.context["action"] == "retrieve":
            ret["assessments"] = [
                {
                    "id": a.id,
                    "date": a.date.strftime("%Y-%m-%d"),
                    "score": a.score,
                    "student_interaction_rating": a.student_interaction_rating,
                    "subjects_taught": a.subjects_taught.values("id", "name"),
                    "notes": a.notes
                }
                for a in instance.assessments.all()
            ]
        return ret


class ActivityAssessmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityAssessment
        fields = "__all__"
        read_only_fields = ("id",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["subjects_taught"] = instance.subjects_taught.values("id", "name")
        return ret


class StudentProgressSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentProgress
        fields = "__all__"
        read_only_fields = ("id",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["subject_class"] = {
            "id": instance.subject_class.id,
            "repr": str(instance.subject_class)
        }
        return ret


class ResourceManagementSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceManagement
        fields = "__all__"
        read_only_fields = ("id",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["allocated_teachers"] = instance.allocated_teachers.values("id", "name")
        return ret


class CoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coach
        fields = "__all__"
        read_only_fields = ("id",)


class CoachTeacherInteractionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoachTeacherInteraction
        fields = "__all__"
        read_only_fields = ("id",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["coach"] = {
            "id": instance.coach.id,
            "name": instance.coach.name
        }
        ret["teacher"] = {
            "id": instance.teacher.id,
            "name": instance.teacher.name
        }
        return ret


class SubjectClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubjectClass
        fields = "__all__"
        read_only_fields = ("id",)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["subject"] = {
            "id": instance.subject.id,
            "name": instance.subject.name
        }
        ret["teacher"] = {
            "id": instance.teacher.id,
            "name": instance.teacher.name
        } if instance.teacher else None
        return ret
