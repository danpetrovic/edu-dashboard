from django.contrib import admin

from core.models import (
    Subject,
    Teacher,
    ActivityAssessment,
    SubjectClass,
    StudentProgress,
    ResourceManagement,
    Coach,
    CoachTeacherInteraction
)


admin.site.register(Subject)
admin.site.register(Teacher)
admin.site.register(ActivityAssessment)
admin.site.register(SubjectClass)
admin.site.register(StudentProgress)
admin.site.register(ResourceManagement)
admin.site.register(Coach)
admin.site.register(CoachTeacherInteraction)
