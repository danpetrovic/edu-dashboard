import datetime
import json
import random

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import models

from core.models import (
    Subject,
    Teacher,
    ActivityAssessment,
    SubjectClass,
    StudentProgress,
    ResourceManagement,
    Coach,
    CoachTeacherInteraction
)

avatars = {
    "John Doe": "teacher-avatars/john-doe.jpg",
    "Mark Brown": "teacher-avatars/mark-brown.jpg",
    "Jane Smith": "teacher-avatars/jane-smith.jpg",
    "Alice Johnson": "teacher-avatars/alice-johnson.jpg",
    "Emily Turner": "coach-avatars/emily-turner.jpg",
    "Michael Rodriguez": "coach-avatars/michael-rodriguez.jpg"
}


class Command(BaseCommand):
    """
    Construct a sample database from the test data.

    The dataset is missing certain items including:
    - there is no way to ascertain class_id for subjects taught by teachers but lacking a student progress record.
    -> Solution: These ids are auto-generated.

    - there is no teacher who teaches science, although science is a subject in the dataset
    -> Solution: A class is created that is missing a teacher (teacher not assigned).
    """

    help = "Load test data for project demo."

    def handle(self, *args, **options):
        Subject.objects.all().delete()
        Teacher.objects.all().delete()
        SubjectClass.objects.all().delete()
        StudentProgress.objects.all().delete()
        ResourceManagement.objects.all().delete()
        Coach.objects.all().delete()
        CoachTeacherInteraction.objects.all().delete()
        with open(f"{settings.BASE_DIR}/dashboard_data.json", "r") as fin:
            dashboard_data = json.loads(fin.read())
        subject_teachers = dict()
        for dat in dashboard_data["teacher_activities"]:
            teacher = Teacher.objects.create(
                id=dat["teacher_id"],
                name=dat["name"],
                date_of_birth=datetime.date(random.randint(1982, 1995), random.randint(1, 12), random.randint(1, 28)),
                avatar=avatars[dat["name"]]
            )
            assessment = ActivityAssessment.objects.create(
                teacher=teacher,
                date=datetime.datetime.strptime(dat["last_active"], "%Y-%m-%d"),
                score=dat["activity_score"],
                student_interaction_rating=dat["student_interaction_rating"]
            )
            for s in dat["subjects_taught"]:
                subject = Subject.objects.get_or_create(name=s)[0]
                assessment.subjects_taught.add(subject)
                subject_teachers[subject] = teacher
        for dat in dashboard_data["student_progress"]:
            subject = Subject.objects.get_or_create(name=dat["subject"])[0]
            teacher = subject_teachers.get(subject, None)
            sc = SubjectClass.objects.create(
                id=dat["class_id"],
                subject=subject,
                teacher=teacher
            )
            spa = StudentProgress.objects.create(
                subject_class=sc,
                description=f"{subject.name} ({teacher.name if teacher else 'No teacher assigned'}) Fall 2022",
                date=datetime.date(2022, 12, 15),
                average_score=random.randint(65, 91),
                homework_completion_rate=dat["homework_completion_rate"],
                attendance_rate=dat["attendance_rate"]
            )
            StudentProgress.objects.create(
                subject_class=sc,
                description=f"{subject.name} ({teacher.name if teacher else 'No teacher assigned'}) Winter 2023",
                date=datetime.date(2023, 4, 15),
                average_score=spa.average_score + dat["average_score_improvement"],
                homework_completion_rate=dat["homework_completion_rate"],
                attendance_rate=dat["attendance_rate"]
            )
        # Input ids are out of sequence, so this ensures they don't conflict.
        auto_id = SubjectClass.objects.aggregate(models.Max("id"))["id__max"] + 1
        for s in Subject.objects.all():
            if not SubjectClass.objects.filter(subject=s).exists():
                t = subject_teachers.get(s, None)
                SubjectClass.objects.create(
                    id=auto_id,
                    subject=s,
                    teacher=t
                )
                auto_id += 1
        for dat in dashboard_data["resource_management"]:
            rm = ResourceManagement.objects.create(
                id=dat["resource_id"],
                name=dat["resource_name"],
                utilization_rate=dat["utilization_rate"]
            )
            rm.allocated_teachers.add(*Teacher.objects.filter(id__in=dat["allocated_teachers"]))
        for dat in dashboard_data["coach_details"]:
            Coach.objects.create(
                id=dat["coach_id"],
                name=dat["name"],
                specialization=dat["specialization"],
                years_of_experience=dat["years_of_experience"],
                avatar=avatars[dat["name"]]
            )
        for dat in dashboard_data["coach_teacher_interactions"]:
            CoachTeacherInteraction.objects.create(
                coach=Coach.objects.get(id=dat["coach_id"]),
                teacher=Teacher.objects.get(id=dat["teacher_id"]),
                meeting_date=datetime.datetime.strptime(dat["last_meeting_date"], "%Y-%m-%d"),
                meeting_notes=dat["meeting_notes"]
            )
