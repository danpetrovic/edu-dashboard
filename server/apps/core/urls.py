from django.urls import path

from rest_framework.routers import DefaultRouter

from core.views import (
    OverviewData,
    SubjectViewset,
    TeacherViewset,
    ActivityAssessmentViewset,
    SubjectClassViewset,
    StudentProgressViewset,
    ResourceManagementViewset,
    CoachViewset,
    CoachTeacherInteractionViewset,
)


router = DefaultRouter(trailing_slash=False)
router.register(r"subject", SubjectViewset, basename="subject")
router.register(r"teacher", TeacherViewset, basename="teacher")
router.register(r"activity-assessment", ActivityAssessmentViewset, basename="activity-assessment")
router.register(r"subject-class", SubjectClassViewset, basename="subject-class")
router.register(r"student-progress", StudentProgressViewset, basename="student-progress")
router.register(r"resource-management", ResourceManagementViewset, basename="resource-management")
router.register(r"coach", CoachViewset, basename="coach")
router.register(r"coach-teacher-interaction", CoachTeacherInteractionViewset, basename="coach-teacher-interaction")

urlpatterns = [
    path("overview-data", OverviewData.as_view(), name="overview-data"),
] + router.urls
