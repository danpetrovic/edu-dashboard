from django.db import models


class Subject(models.Model):
    """
    E.g. mathematics, history, chemistry, etc.
    """

    name = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class Teacher(models.Model):
    """
    An individual teacher.
    """

    name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True)
    avatar = models.ImageField(null=True, blank=True, upload_to="teacher-avatars")
    notes = models.TextField(blank=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name


class ActivityAssessment(models.Model):
    """
    Record metrics from a teachers assessment program.
    """

    teacher = models.ForeignKey("core.teacher", on_delete=models.CASCADE, related_name="assessments")
    date = models.DateField()
    score = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    student_interaction_rating = models.DecimalField(max_digits=2, decimal_places=1, null=True)
    subjects_taught = models.ManyToManyField("core.subject")
    notes = models.TextField(blank=True)

    class Meta:
        ordering = ("-date",)

    def __str__(self):
        return f"{self.date.strftime('%Y-%m-%d')} - {self.teacher}"


class SubjectClass(models.Model):
    """
    Instance of a subject being taught by a particular teacher.
    """

    subject = models.ForeignKey("core.subject", on_delete=models.CASCADE)
    teacher = models.ForeignKey("core.teacher", on_delete=models.SET_NULL, null=True, related_name="classes")
    notes = models.TextField(blank=True)

    class Meta:
        verbose_name = "Class"
        verbose_name_plural = "Classes"
        ordering = ("subject",)

    def __str__(self):
        return f"{self.subject} ({self.teacher or 'No teacher assigned'})"


class StudentProgress(models.Model):
    """
    Record metrics about the student performance in a particular class.
    """

    subject_class = models.ForeignKey("core.SubjectClass", on_delete=models.CASCADE, related_name="student_progress")
    description = models.CharField(max_length=100)
    date = models.DateField()
    average_score = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    homework_completion_rate = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    attendance_rate = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    notes = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = "Student progress"
        ordering = ("-date",)

    def __str__(self):
        return f"{self.subject_class}"


class ResourceManagement(models.Model):
    """
    Record of a teaching resource and the degree to which teachers are using it.
    """

    name = models.CharField(max_length=200)
    allocated_teachers = models.ManyToManyField("core.teacher")
    utilization_rate = models.DecimalField(max_digits=5, decimal_places=2, null=True)

    class Meta:
        verbose_name_plural = "Resource management"
        ordering = ("name",)

    def __str__(self):
        return self.name


class Coach(models.Model):
    """
    A teaching coach and their details.
    """

    name = models.CharField(max_length=100)
    specialization = models.CharField(max_length=200)
    years_of_experience = models.IntegerField(null=True)
    avatar = models.ImageField(null=True, blank=True, upload_to="coach-avatars")

    class Meta:
        verbose_name_plural = "Coaches"
        ordering = ("name",)

    def __str__(self):
        return self.name


class CoachTeacherInteraction(models.Model):
    """
    Record of a meeting (coaching session?) between a coach and a teacher.
    """

    coach = models.ForeignKey("core.coach", on_delete=models.CASCADE, related_name="teacher_meetings")
    teacher = models.ForeignKey("core.teacher", on_delete=models.CASCADE, related_name="coach_meetings")
    meeting_date = models.DateField()
    meeting_notes = models.TextField()

    class Meta:
        ordering = ("-meeting_date",)

    def __str__(self):
        return f"{self.coach} and {self.teacher}"
