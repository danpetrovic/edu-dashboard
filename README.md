
# Educational Dashboard Sample Project


Project Description:

A simplified educational dashboard for tracking teacher activities, student progress, and resource management.

![Overview](./overview-light.png "Overview Page"){height=200px}&nbsp;&nbsp;![Overview](./overview-dark.png "Overview Page"){height=200px}  
Light & Dark Themes

To run, make sure the following are installed:

- git
- docker (docker desktop or at least have a docker daemon running)

Then, clone the project repository:

```
git clone https://gitlab.com/danpetrovic/edu-dashboard.git
```

Navigate to the root folder containing the Dockerfile and the client and server folders.

Run the following command to build the docker image:

```
docker build -t edu .
```

This may take several minutes, depending on your hardware and whether the image was previously cached.

Finally, run the docker container (this may also take a couple of minutes to start up fully):

```
docker run -p 8000:8000 -p 4200:4200 edu
```

Open a browser window and navigate to http://localhost:4200 to view the application.