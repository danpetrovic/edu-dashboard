import { Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs';
import { ApiService } from '../api.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-teacher-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatButtonModule,
    JoinPipe,
  ],
  templateUrl: './teacher-list.component.html',
  styleUrl: './teacher-list.component.scss'
})
export class TeacherListComponent implements OnInit {

  teachers: any;
  query = new FormControl('');

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.teachers = result['teacherListPage'].results;
    });
    this.query.valueChanges.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(() => this.api.getTeacherList(this.query.value || undefined))
    ).subscribe(page => {
      this.teachers = page.results;
    });
  }
}
