import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-resource-management-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
    MatListModule,
    MatRippleModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './resource-management-detail.component.html',
  styleUrl: './resource-management-detail.component.scss'
})
export class ResourceManagementDetailComponent implements OnInit {
  
  instance: any;

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.instance = result['instance'];
    });
  }
  getAllocatedTeachers(): string {
    return this.instance.allocated_teachers.map((v: any) => v.name).join(', ');
  }

  del(): void {
    this.api.deleteResourceManagement(this.instance.id).subscribe(() => {
      this.snackbar.open('Resource deleted.', undefined, {
        duration: 3000
      });
      this.router.navigate(['/classes']);
    });
  }
}
