import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceManagementDetailComponent } from './resource-management-detail.component';

describe('ResourceManagementDetailComponent', () => {
  let component: ResourceManagementDetailComponent;
  let fixture: ComponentFixture<ResourceManagementDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResourceManagementDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ResourceManagementDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
