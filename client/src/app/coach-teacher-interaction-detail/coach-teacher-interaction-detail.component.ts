import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-coach-teacher-interaction-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
    MatListModule,
    MatRippleModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './coach-teacher-interaction-detail.component.html',
  styleUrl: './coach-teacher-interaction-detail.component.scss'
})
export class CoachTeacherInteractionDetailComponent implements OnInit {
  
  instance: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private snackbar: MatSnackBar,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.instance = result['instance'];
    });
  }

  del(): void {
    this.api.deleteCoachTeacherInteraction(this.instance.id).subscribe(() => {
      this.snackbar.open('Interaction deleted.', undefined, {
        duration: 3000
      });
      this.router.navigate(['/coaching']);
    });
  }
}
