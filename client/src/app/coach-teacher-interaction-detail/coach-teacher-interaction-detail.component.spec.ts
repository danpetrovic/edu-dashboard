import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachTeacherInteractionDetailComponent } from './coach-teacher-interaction-detail.component';

describe('CoachTeacherInteractionDetailComponent', () => {
  let component: CoachTeacherInteractionDetailComponent;
  let fixture: ComponentFixture<CoachTeacherInteractionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoachTeacherInteractionDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CoachTeacherInteractionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
