import { Component, EventEmitter, Inject, OnDestroy, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, MomentDateModule } from '@angular/material-moment-adapter';
import { ApiService } from '../api.service';
import { MatSelectModule } from '@angular/material/select';
import { Subject, takeUntil } from 'rxjs';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-activity-assessment-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MomentDateModule,
    JoinPipe
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: ['YYYY-MM-DD']
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
        }
      }
    },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
  ],
  templateUrl: './activity-assessment-form.component.html',
  styleUrl: './activity-assessment-form.component.scss'
})
export class ActivityAssessmentFormComponent implements OnInit, OnDestroy {

  @Output() formSubmitted = new EventEmitter();

  subjects: any;
  instance: any;
  teacher: number;
  form = new FormGroup({
    date: new FormControl(null, Validators.required),
    score: new FormControl(null, Validators.required),
    student_interaction_rating: new FormControl(null, Validators.required),
    subjects_taught: new FormControl([], Validators.required),
    notes: new FormControl('')
  });
  date = new FormControl(null);
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private dialogRef: MatDialogRef<ActivityAssessmentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.instance = data.instance;
    this.teacher = data.teacher;
  }

  ngOnInit(): void {
    this.api.getSubjectList().pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(page => {
      this.subjects = page.results;
      if (this.instance) {
        this.form.setValue({
          date: this.instance.date,
          score: this.instance.score,
          student_interaction_rating: this.instance.student_interaction_rating,
          subjects_taught: this.instance.subjects_taught,
          notes: this.instance.notes
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  compareSelectObjects(o1: any, o2: any): boolean {
    return o1?.id === o2?.id;
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const dt = typeof this.form.value.date === 'string' ? this.form.value.date : (<any>this.form.value.date).format('YYYY-MM-DD');
    const data = {
      teacher: this.teacher,
      date: dt,
      score: this.form.value.score,
      student_interaction_rating: this.form.value.student_interaction_rating,
      subjects_taught: this.form.value.subjects_taught!.map((v: any) => v.id),
      notes: this.form.value.notes
    };
    if (this.instance) {
      this.api.updateActivityAssessment(this.instance.id, data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(result => {
        this.snackbar.open('Activity updated.', undefined, {
          duration: 3000
        });
        this.formSubmitted.emit();
      });
    } else {
      this.api.createActivityAssessment(data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(result => {
        this.snackbar.open('Activity created.', undefined, {
          duration: 3000
        });
        this.formSubmitted.emit();
      });
    }
  }
}
