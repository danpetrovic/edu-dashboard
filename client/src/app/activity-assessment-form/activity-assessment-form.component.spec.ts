import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityAssessmentFormComponent } from './activity-assessment-form.component';

describe('ActivityAssessmentFormComponent', () => {
  let component: ActivityAssessmentFormComponent;
  let fixture: ComponentFixture<ActivityAssessmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActivityAssessmentFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActivityAssessmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
