import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { ActivatedRoute } from '@angular/router';
import { JoinPipe, MediaUrlCssPipe } from '../pipes';
import { NgChartsModule } from 'ng2-charts';
import { ChartConfiguration } from 'chart.js';

@Component({
  selector: 'app-overview',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatChipsModule,
    NgChartsModule,
    JoinPipe,
    MediaUrlCssPipe
  ],
  templateUrl: './overview.component.html',
  styleUrl: './overview.component.scss'
})
export class OverviewComponent implements OnInit {

  dashboardData: any;
  resourceManagementOptions: ChartConfiguration<'bar'>['options'] = {
    plugins: {
      legend: {
        display: true,
      },
      tooltip: {
        enabled: true,
        callbacks: {
          title: context => {
            const dataObj = this.dashboardData.resource_management[context[0].datasetIndex];
            return `${dataObj.resource_name}`;
          },
          label: context => {
            return `${context.raw}% Utilization`
          },
          footer: context => {
            const dataObj = this.dashboardData.resource_management[context[0].datasetIndex];
            const allocatedTeachers = dataObj.allocated_teachers.map((v: any) => v.name).join(', ');
            return `Allocated Teachers: ${allocatedTeachers}`;
          }
        }
      },
      colors: {
        enabled: true,
      }
    },
  };
  resourceManagementData: ChartConfiguration<'bar'>['data'] = { labels: ['Teaching Resources'], datasets: [] };


  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.dashboardData = result['data'];
      for (let resource of this.dashboardData.resource_management) {
        this.resourceManagementData.datasets.push({
          label: resource.resource_name,
          data: [resource.utilization_rate],
        });
      }
    });
  }
}
