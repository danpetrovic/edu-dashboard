import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectClassDetailComponent } from './subject-class-detail.component';

describe('SubjectClassDetailComponent', () => {
  let component: SubjectClassDetailComponent;
  let fixture: ComponentFixture<SubjectClassDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SubjectClassDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SubjectClassDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
