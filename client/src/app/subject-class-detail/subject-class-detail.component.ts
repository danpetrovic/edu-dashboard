import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-subject-class-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
    MatListModule,
    MatRippleModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './subject-class-detail.component.html',
  styleUrl: './subject-class-detail.component.scss'
})
export class SubjectClassDetailComponent implements OnInit {
  
  instance: any;

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.instance = result['instance'];
    });
  }

  del(): void {
    this.api.deleteSubjectClass(this.instance.id).subscribe(() => {
      this.snackbar.open('Class deleted.', undefined, {
        duration: 3000
      });
      this.router.navigate(['/classes']);
    });
  }
}
