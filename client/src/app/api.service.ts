import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  getOverviewData(): Observable<any> {
    const url = `${environment.apiUrl}/overview-data`;
    return this.http.get<any>(url);
  }

  getSubjectList(): Observable<any> {
    const url = `${environment.apiUrl}/subject`;
    return this.http.get<any>(url);
  }

  getSubject(subjectId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/subject/${subjectId}`;
    return this.http.get<any>(url);
  }

  createSubject(data: any): Observable<any> {
    const url = `${environment.apiUrl}/subject`;
    return this.http.post<any>(url, data);
  }

  updateSubject(subjectId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/subject/${subjectId}`;
    return this.http.put<any>(url, data);
  }

  deleteSubject(subjectId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/subject/${subjectId}`;
    return this.http.delete<any>(url);
  }

  getTeacherList(query?: string): Observable<any> {
    const url = `${environment.apiUrl}/teacher`;
    const options = { params: new HttpParams() }
    if (query !== undefined) {
      options.params = options.params.set('q', query);
    }
    return this.http.get<any>(url, options);
  }

  getTeacher(teacherId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/teacher/${teacherId}`;
    return this.http.get<any>(url);
  }

  createTeacher(data: any): Observable<any> {
    const url = `${environment.apiUrl}/teacher`;
    return this.http.post<any>(url, data);
  }

  updateTeacher(teacherId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/teacher/${teacherId}`;
    return this.http.put<any>(url, data);
  }

  deleteTeacher(teacherId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/teacher/${teacherId}`;
    return this.http.delete<any>(url);
  }

  getActivityAssessmentList(teacherId?: number): Observable<any> {
    const url = `${environment.apiUrl}/activity-assessment`;
    const options = { params: new HttpParams() }
    if (teacherId !== undefined) {
      options.params = options.params.set('t', teacherId);
    }
    return this.http.get<any>(url, options);
  }

  getActivityAssessment(activityAssessmentId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/activity-assessment/${activityAssessmentId}`;
    return this.http.get<any>(url);
  }

  createActivityAssessment(data: any): Observable<any> {
    const url = `${environment.apiUrl}/activity-assessment`;
    return this.http.post<any>(url, data);
  }

  updateActivityAssessment(activityAssessmentId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/activity-assessment/${activityAssessmentId}`;
    return this.http.put<any>(url, data);
  }

  deleteActivityAssessment(activityAssessmentId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/activity-assessment/${activityAssessmentId}`;
    return this.http.delete<any>(url);
  }

  getSubjectClassList(): Observable<any> {
    const url = `${environment.apiUrl}/subject-class`;
    return this.http.get<any>(url);
  }

  getSubjectClass(subjectClassId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/subject-class/${subjectClassId}`;
    return this.http.get<any>(url);
  }

  createSubjectClass(data: any): Observable<any> {
    const url = `${environment.apiUrl}/subject-class`;
    return this.http.post<any>(url, data);
  }

  updateSubjectClass(subjectClassId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/subject-class/${subjectClassId}`;
    return this.http.put<any>(url, data);
  }

  deleteSubjectClass(subjectClassId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/subject-class/${subjectClassId}`;
    return this.http.delete<any>(url);
  }

  getStudentProgressList(subject?: any, teacher?: any): Observable<any> {
    const url = `${environment.apiUrl}/student-progress`;
    const options = { params: new HttpParams() };
    if (subject !== undefined) {
      options.params = options.params.set('subject', subject);
    }
    if (teacher !== undefined) {
      options.params = options.params.set('teacher', teacher);
    }
    return this.http.get<any>(url, options);
  }

  getStudentProgress(studentProgressId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/student-progress/${studentProgressId}`;
    return this.http.get<any>(url);
  }

  createStudentProgress(data: any): Observable<any> {
    const url = `${environment.apiUrl}/student-progress`;
    return this.http.post<any>(url, data);
  }

  updateStudentProgress(studentProgressId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/student-progress/${studentProgressId}`;
    return this.http.put<any>(url, data);
  }

  deleteStudentProgress(studentProgressId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/student-progress/${studentProgressId}`;
    return this.http.delete<any>(url);
  }

  getResourceManagementList(): Observable<any> {
    const url = `${environment.apiUrl}/resource-management`;
    return this.http.get<any>(url);
  }

  getResourceManagement(resourceManagementId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/resource-management/${resourceManagementId}`;
    return this.http.get<any>(url);
  }

  createResourceManagement(data: any): Observable<any> {
    const url = `${environment.apiUrl}/resource-management`;
    return this.http.post<any>(url, data);
  }

  updateResourceManagement(resourceManagementId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/resource-management/${resourceManagementId}`;
    return this.http.put<any>(url, data);
  }

  deleteResourceManagement(resourceManagementId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/resource-management/${resourceManagementId}`;
    return this.http.delete<any>(url);
  }

  getCoachList(): Observable<any> {
    const url = `${environment.apiUrl}/coach`;
    return this.http.get<any>(url);
  }

  getCoach(coachId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/coach/${coachId}`;
    return this.http.get<any>(url);
  }

  createCoach(data: any): Observable<any> {
    const url = `${environment.apiUrl}/coach`;
    return this.http.post<any>(url, data);
  }

  updateCoach(coachId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/coach/${coachId}`;
    return this.http.put<any>(url, data);
  }

  deleteCoach(coachId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/coach/${coachId}`;
    return this.http.delete<any>(url);
  }

  getCoachTeacherInteractionList(): Observable<any> {
    const url = `${environment.apiUrl}/coach-teacher-interaction`;
    return this.http.get<any>(url);
  }

  getCoachTeacherInteraction(coachTeacherInteractionId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/coach-teacher-interaction/${coachTeacherInteractionId}`;
    return this.http.get<any>(url);
  }

  createCoachTeacherInteraction(data: any): Observable<any> {
    const url = `${environment.apiUrl}/coach-teacher-interaction`;
    return this.http.post<any>(url, data);
  }

  updateCoachTeacherInteraction(coachTeacherInteractionId: string|number, data: any): Observable<any> {
    const url = `${environment.apiUrl}/coach-teacher-interaction/${coachTeacherInteractionId}`;
    return this.http.put<any>(url, data);
  }

  deleteCoachTeacherInteraction(coachTeacherInteractionId: string|number): Observable<any> {
    const url = `${environment.apiUrl}/coach-teacher-interaction/${coachTeacherInteractionId}`;
    return this.http.delete<any>(url);
  }
}
