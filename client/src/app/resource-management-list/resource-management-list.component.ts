import { Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { ApiService } from '../api.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-resource-management-list',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    JoinPipe,
  ],
  templateUrl: './resource-management-list.component.html',
  styleUrl: './resource-management-list.component.scss'
})
export class ResourceManagementListComponent implements OnInit {

  resourceManagementList: any;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.resourceManagementList = result['resourceManagementListPage'].results;
    });
  }

  getAllocatedTeachers(resourceMangement: any): string {
    return resourceMangement.allocated_teachers.map((v: any) => v.name).join(', ');
  }
}
