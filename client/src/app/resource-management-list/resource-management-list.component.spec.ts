import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceManagementListComponent } from './resource-management-list.component';

describe('ResourceManagementListComponent', () => {
  let component: ResourceManagementListComponent;
  let fixture: ComponentFixture<ResourceManagementListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResourceManagementListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ResourceManagementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
