import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { ApiService } from '../api.service';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { CoachDetailComponent } from '../coach-detail/coach-detail.component';
import { Subject, takeUntil } from 'rxjs';
import { CoachFormComponent } from '../coach-form/coach-form.component';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-coach-teacher-interaction-list',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatIconModule,
    MatRippleModule,
    MatSnackBarModule,
    MatButtonModule,
    JoinPipe,
  ],
  templateUrl: './coach-teacher-interaction-list.component.html',
  styleUrl: './coach-teacher-interaction-list.component.scss'
})
export class CoachTeacherInteractionListComponent implements OnInit, OnDestroy {

  coaches: any;
  interactions: any;

  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private route: ActivatedRoute,
    private api: ApiService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.coaches = result['coachListPage'].results;
      this.interactions = result['coachTeacherInteractionListPage'].results;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  openCoachDetail(coach: any): void {
    const dialogRef = this.dialog.open(CoachDetailComponent, {
      data: { instance: coach },
      autoFocus: false,
      minWidth: '400px'
    });
    dialogRef.componentInstance.edit.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(() => {
      dialogRef.afterClosed().pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.openCoachForm(coach);
      });
      dialogRef.close();
    });
    dialogRef.componentInstance.del.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(() => {
      this.api.deleteCoach(coach.id).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.api.getCoachList().pipe(
          takeUntil(this.unsubscribe)
        ).subscribe(page => {
          this.coaches = page.results;
        });
        this.api.getCoachTeacherInteractionList().pipe(
          takeUntil(this.unsubscribe)
        ).subscribe(page => {
          this.interactions = page.results;
        });
        this.snackbar.open('Coach deleted.', undefined, {
          duration: 3000
        });
        dialogRef.close();
      });
    })
  }

  openCoachForm(coach?: any): void {
    const dialogRef = this.dialog.open(CoachFormComponent, {
      data: { instance: coach },
      autoFocus: false,
      minWidth: '400px'
    });
    dialogRef.componentInstance.formSubmitted.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(() => {
      this.api.getCoachList().pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(page => {
        this.coaches = page.results;
      });
      this.api.getCoachTeacherInteractionList().pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(page => {
        this.interactions = page.results;
      });
      dialogRef.close();
    });
  }
}
