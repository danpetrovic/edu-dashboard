import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachTeacherInteractionListComponent } from './coach-teacher-interaction-list.component';

describe('CoachTeacherInteractionListComponent', () => {
  let component: CoachTeacherInteractionListComponent;
  let fixture: ComponentFixture<CoachTeacherInteractionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoachTeacherInteractionListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CoachTeacherInteractionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
