import { Routes } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { coachListResolver, coachTeacherInteractionListResolver, coachTeacherInteractionResolver, overviewResolver, resourceManagementListResolver, resourceManagementResolver, studentProgressListResolver, studentProgressResolver, subjectClassListResolver, subjectClassResolver, subjectListResolver, teacherListResolver, teacherResolver } from './resolvers';
import { TeacherListComponent } from './teacher-list/teacher-list.component';
import { StudentProgressListComponent } from './student-progress-list/student-progress-list.component';
import { TeacherDetailComponent } from './teacher-detail/teacher-detail.component';
import { StudentProgressDetailComponent } from './student-progress-detail/student-progress-detail.component';
import { ResourceManagementListComponent } from './resource-management-list/resource-management-list.component';
import { ResourceManagementDetailComponent } from './resource-management-detail/resource-management-detail.component';
import { SubjectClassDetailComponent } from './subject-class-detail/subject-class-detail.component';
import { SubjectClassListComponent } from './subject-class-list/subject-class-list.component';
import { CoachTeacherInteractionListComponent } from './coach-teacher-interaction-list/coach-teacher-interaction-list.component';
import { CoachTeacherInteractionDetailComponent } from './coach-teacher-interaction-detail/coach-teacher-interaction-detail.component';
import { TeacherFormComponent } from './teacher-form/teacher-form.component';
import { StudentProgressFormComponent } from './student-progress-form/student-progress-form.component';
import { CoachTeacherInteractionFormComponent } from './coach-teacher-interaction-form/coach-teacher-interaction-form.component';
import { SubjectClassFormComponent } from './subject-class-form/subject-class-form.component';
import { ResourceManagementFormComponent } from './resource-management-form/resource-management-form.component';

export const routes: Routes = [
  {
    path: '',
    component: OverviewComponent,
    resolve: {
      data: overviewResolver
    }
  },
  {
    path: 'teachers',
    component: TeacherListComponent,
    resolve: {
      teacherListPage: teacherListResolver
    }
  },
  {
    path: 'teachers/new',
    component: TeacherFormComponent,
  },
  {
    path: 'teachers/:teacherId',
    component: TeacherDetailComponent,
    resolve: {
      instance: teacherResolver
    }
  },
  {
    path: 'teachers/:teacherId/edit',
    component: TeacherFormComponent,
    resolve: {
      instance: teacherResolver
    }
  },
  {
    path: 'classes',
    component: SubjectClassListComponent,
    resolve: {
      subjectClassListPage: subjectClassListResolver
    }
  },
  {
    path: 'classes/new',
    component: SubjectClassFormComponent,
    resolve: {
      subjectListPage: subjectListResolver,
      teacherListPage: teacherListResolver,
    }
  },
  {
    path: 'classes/:subjectClassId',
    component: SubjectClassDetailComponent,
    resolve: {
      instance: subjectClassResolver
    }
  },
  {
    path: 'classes/:subjectClassId/edit',
    component: SubjectClassFormComponent,
    resolve: {
      subjectListPage: subjectListResolver,
      teacherListPage: teacherListResolver,
      instance: subjectClassResolver
    }
  },
  {
    path: 'student-progress',
    component: StudentProgressListComponent,
    resolve: {
      teacherListPage: teacherListResolver,
      subjectListPage: subjectListResolver,
      studentProgressListPage: studentProgressListResolver
    }
  },
  {
    path: 'student-progress/new',
    component: StudentProgressFormComponent,
    resolve: {
      subjectClassListPage: subjectClassListResolver
    }
  },
  {
    path: 'student-progress/:studentProgressId',
    component: StudentProgressDetailComponent,
    resolve: {
      instance: studentProgressResolver
    }
  },
  {
    path: 'student-progress/:studentProgressId/edit',
    component: StudentProgressFormComponent,
    resolve: {
      subjectClassListPage: subjectClassListResolver,
      instance: studentProgressResolver
    }
  },
  {
    path: 'resource-management',
    component: ResourceManagementListComponent,
    resolve: {
      resourceManagementListPage: resourceManagementListResolver
    }
  },
  {
    path: 'resource-management/new',
    component: ResourceManagementFormComponent,
    resolve: {
      teacherListPage: teacherListResolver
    }
  },
  {
    path: 'resource-management/:resourceManagementId',
    component: ResourceManagementDetailComponent,
    resolve: {
      instance: resourceManagementResolver
    }
  },
  {
    path: 'resource-management/:resourceManagementId/edit',
    component: ResourceManagementFormComponent,
    resolve: {
      teacherListPage: teacherListResolver,
      instance: resourceManagementResolver
    }
  },
  {
    path: 'coaching',
    component: CoachTeacherInteractionListComponent,
    resolve: {
      coachListPage: coachListResolver,
      coachTeacherInteractionListPage: coachTeacherInteractionListResolver
    }
  },
  {
    path: 'coaching/new',
    component: CoachTeacherInteractionFormComponent,
    resolve: {
      coachListPage: coachListResolver,
      teacherListPage: teacherListResolver,
    }
  },
  {
    path: 'coaching/:coachTeacherInteractionId',
    component: CoachTeacherInteractionDetailComponent,
    resolve: {
      instance: coachTeacherInteractionResolver
    }
  },
  {
    path: 'coaching/:coachTeacherInteractionId/edit',
    component: CoachTeacherInteractionFormComponent,
    resolve: {
      coachListPage: coachListResolver,
      teacherListPage: teacherListResolver,
      instance: coachTeacherInteractionResolver
    }
  },
];
