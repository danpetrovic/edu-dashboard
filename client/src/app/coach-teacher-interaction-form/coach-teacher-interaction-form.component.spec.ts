import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachTeacherInteractionFormComponent } from './coach-teacher-interaction-form.component';

describe('CoachTeacherInteractionFormComponent', () => {
  let component: CoachTeacherInteractionFormComponent;
  let fixture: ComponentFixture<CoachTeacherInteractionFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoachTeacherInteractionFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CoachTeacherInteractionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
