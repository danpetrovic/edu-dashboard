import { Component, OnDestroy, OnInit } from '@angular/core';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../api.service';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, MomentDateModule } from '@angular/material-moment-adapter';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { Subject, takeUntil } from 'rxjs';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-coach-teacher-interaction-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatDatepickerModule,
    MatSnackBarModule,
    MomentDateModule,
    JoinPipe
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: ['YYYY-MM-DD']
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
        }
      }
    },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
  ],
  templateUrl: './coach-teacher-interaction-form.component.html',
  styleUrl: './coach-teacher-interaction-form.component.scss'
})
export class CoachTeacherInteractionFormComponent implements OnInit, OnDestroy {

  coaches: any;
  teachers: any;
  instance: any;
  form = new FormGroup({
    coach: new FormControl(null, Validators.required),
    teacher: new FormControl(null, Validators.required),
    meeting_date: new FormControl(null, Validators.required),
    meeting_notes: new FormControl('')
  });
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.coaches = result['coachListPage'].results;
      this.teachers = result['teacherListPage'].results;
      if (result['instance']) {
        this.instance = result['instance'];
        this.form.setValue({
          coach: this.instance.coach,
          teacher: this.instance.teacher,
          meeting_date: this.instance.meeting_date,
          meeting_notes: this.instance.meeting_notes
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  compareSelectObjects(o1: any, o2: any): boolean {
    return o1?.id === o2?.id;
  }

  cancel(): void {
    this.router.navigate(['/coaching'])
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const dt = typeof this.form.value.meeting_date === 'string' ? this.form.value.meeting_date : (<any>this.form.value.meeting_date).format('YYYY-MM-DD');
    const data = {
      coach: (<any>this.form.value.coach).id,
      teacher: (<any>this.form.value.teacher).id,
      meeting_date: dt,
      meeting_notes: this.form.value.meeting_notes
    };
    if (this.instance) {
      this.api.updateCoachTeacherInteraction(this.instance.id, data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Coach teacher interaction updated.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/coaching'])
      });
    } else {
      this.api.createCoachTeacherInteraction(data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Coach teacher interaction created.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/coaching'])
      });
    }
  }
}
