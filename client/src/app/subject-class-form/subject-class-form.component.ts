import { Component, OnDestroy, OnInit } from '@angular/core';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { Subject, takeUntil } from 'rxjs';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-subject-class-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './subject-class-form.component.html',
  styleUrl: './subject-class-form.component.scss'
})
export class SubjectClassFormComponent implements OnInit, OnDestroy {

  subjects: any;
  teachers: any;
  instance: any;
  form = new FormGroup({
    subject: new FormControl(null, Validators.required),
    teacher: new FormControl(null, Validators.required),
    notes: new FormControl('')
  });
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.subjects = result['subjectListPage'].results;
      this.teachers = result['teacherListPage'].results;
      if (result['instance']) {
        this.instance = result['instance'];
        this.form.setValue({
          subject: this.instance.subject,
          teacher: this.instance.teacher,
          notes: this.instance.notes
        });
      }
    })
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  compareSelectObjects(o1: any, o2: any): boolean {
    return o1?.id === o2?.id;
  }

  cancel(): void {
    this.router.navigate(['/classes']);
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const data = {
      subject: (<any>this.form.value.subject).id,
      teacher: (<any>this.form.value.teacher).id,
      notes: this.form.value.notes
    };
    if (this.instance) {
      this.api.updateSubjectClass(this.instance.id, data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Class updated.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/classes']);
      });
    } else {
      this.api.createSubjectClass(data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Class created.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/classes']);
      });
    }
  }
}
