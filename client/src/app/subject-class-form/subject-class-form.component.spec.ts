import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectClassFormComponent } from './subject-class-form.component';

describe('SubjectClassFormComponent', () => {
  let component: SubjectClassFormComponent;
  let fixture: ComponentFixture<SubjectClassFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SubjectClassFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SubjectClassFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
