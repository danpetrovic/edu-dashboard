import { Injectable } from '@angular/core';
import { ThemeService as ChartsThemeService } from 'ng2-charts';
import { ChartOptions } from 'chart.js';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  public mode: 'light'|'dark' = 'light';

  constructor(
    private chartsTheme: ChartsThemeService
  ) { }

  setTheme(theme: 'light'|'dark'): void {
    const body: any = document.getElementsByTagName('body')[0];
    body.dataset.theme = theme;
    this.mode = theme;
    this.notifyChartsThemeService();
  }

  private notifyChartsThemeService(): void {
    let textColor, gridColor;
    if (this.mode === 'light') {
      textColor = '#666666';
      gridColor = 'rgba(0, 0, 0, 0.1)';
    } else {
      textColor = '#efefef';
      gridColor = 'rgba(255, 255, 255, 0.1)';
    }
    const overrides: ChartOptions = {
      scales: {
        x: {
          ticks: {
            color: textColor
          },
          grid: {
            color: gridColor
          }
        },
        y: {
          ticks: {
            color: textColor
          },
          grid: {
            color: gridColor
          }
        }
      },
      plugins: {
        legend: {
          display: true,
          labels: {
            color: textColor
          }
        },
      }
    }
    this.chartsTheme.setColorschemesOptions(overrides);
  }
}
