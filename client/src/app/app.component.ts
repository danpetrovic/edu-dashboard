import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavigationStart, Router, RouterEvent, RouterOutlet } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ThemeService } from './theme.service';
import { MatDrawerMode, MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleChange, MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { filter } from 'rxjs';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    RouterOutlet,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatListModule,
    MatRippleModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'client';
  sideNavOpen: boolean = true;
  sideNavMode: MatDrawerMode = 'side';

  constructor(
    public theme: ThemeService,
    public router: Router,
    public breakpoints: BreakpointObserver
  ) {
    const widthQuery = '(max-width: 600px)';
    const sizeChanges = breakpoints.observe(widthQuery);
    sizeChanges.subscribe(result => {
      if (result.breakpoints[widthQuery]) {
        this.sideNavOpen = false;
        this.sideNavMode = 'over';
      } else {
        this.sideNavOpen = true;
        this.sideNavMode = 'side';
      }
    });
    router.events.pipe(
      filter((e: any): e is RouterEvent => e instanceof RouterEvent)
    ).subscribe((e: RouterEvent) => {
      // Close the sidenav when navigation to a different route begins.
      if (e instanceof NavigationStart && this.sideNavMode === 'over') {
        this.sideNavOpen = false;
      }
    });
  }

  switchTheme(e: MatSlideToggleChange): void {
    if (e.checked) {
      this.theme.setTheme('dark');
    } else {
      this.theme.setTheme('light');
    }
  }
}
