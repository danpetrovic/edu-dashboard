import { Component, OnDestroy, OnInit } from '@angular/core';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../api.service';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, MomentDateModule } from '@angular/material-moment-adapter';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { Subject, takeUntil } from 'rxjs';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-student-progress-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatDatepickerModule,
    MatSnackBarModule,
    MomentDateModule,
    JoinPipe
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: ['YYYY-MM-DD']
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
        }
      }
    },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
  ],
  templateUrl: './student-progress-form.component.html',
  styleUrl: './student-progress-form.component.scss'
})
export class StudentProgressFormComponent implements OnInit, OnDestroy {

  subjectClasses: any;
  instance: any;
  form = new FormGroup({
    subject_class: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    average_score: new FormControl(null, Validators.required),
    homework_completion_rate: new FormControl(null, Validators.required),
    attendance_rate: new FormControl(null, Validators.required)
  });
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.subjectClasses = result['subjectClassListPage'].results;
      if (result['instance']) {
        this.instance = result['instance'];
        this.form.setValue({
          subject_class: this.instance.subject_class,
          description: this.instance.description,
          date: this.instance.date,
          average_score: this.instance.average_score,
          homework_completion_rate: this.instance.homework_completion_rate,
          attendance_rate: this.instance.attendance_rate
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  compareSelectObjects(o1: any, o2: any): boolean {
    return o1?.id === o2?.id;
  }

  cancel(): void {
    this.router.navigate(['/student-progress'])
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const dt = typeof this.form.value.date === 'string' ? this.form.value.date : (<any>this.form.value.date).format('YYYY-MM-DD');
    const data = {
      subject_class: (<any>this.form.value.subject_class).id,
      description: this.form.value.description,
      date: dt,
      average_score: this.form.value.average_score,
      homework_completion_rate: this.form.value.homework_completion_rate,
      attendance_rate: this.form.value.attendance_rate
    };
    if (this.instance) {
      this.api.updateStudentProgress(this.instance.id, data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(result => {
        this.snackbar.open('Student progress updated.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/student-progress']);
      });
    } else {
      this.api.createStudentProgress(data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(result => {
        this.snackbar.open('Student progress created.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/student-progress']);
      });
    }
  }
}
