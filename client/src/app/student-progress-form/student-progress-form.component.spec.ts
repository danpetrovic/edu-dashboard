import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentProgressFormComponent } from './student-progress-form.component';

describe('StudentProgressFormComponent', () => {
  let component: StudentProgressFormComponent;
  let fixture: ComponentFixture<StudentProgressFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentProgressFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentProgressFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
