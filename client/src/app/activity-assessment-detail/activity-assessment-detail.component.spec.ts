import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityAssessmentDetailComponent } from './activity-assessment-detail.component';

describe('ActivityAssessmentDetailComponent', () => {
  let component: ActivityAssessmentDetailComponent;
  let fixture: ComponentFixture<ActivityAssessmentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActivityAssessmentDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ActivityAssessmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
