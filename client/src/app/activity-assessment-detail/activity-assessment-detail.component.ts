import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogTitle } from '@angular/material/dialog';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-activity-assessment-detail',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './activity-assessment-detail.component.html',
  styleUrl: './activity-assessment-detail.component.scss'
})
export class ActivityAssessmentDetailComponent {

  @Output() del = new EventEmitter();
  @Output() edit = new EventEmitter();

  instance: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.instance = data.instance;
  }

  getSubjectsTaughtDisplay(assessment: any): string {
    return assessment.subjects_taught.map((v: any) => v.name).join(', ');
  }
}
