import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { Subject } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-student-progress-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
    MatListModule,
    MatRippleModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './student-progress-detail.component.html',
  styleUrl: './student-progress-detail.component.scss'
})
export class StudentProgressDetailComponent implements OnInit, OnDestroy {

  instance: any;
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.instance = result['instance'];
    })
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  del(): void {
    this.api.deleteStudentProgress(this.instance.id).subscribe(() => {
      this.snackbar.open('Student progress deleted.', undefined, {
        duration: 3000
      });
      this.router.navigate(['/student-progress']);
    });
  }
}
