import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentProgressDetailComponent } from './student-progress-detail.component';

describe('StudentProgressDetailComponent', () => {
  let component: StudentProgressDetailComponent;
  let fixture: ComponentFixture<StudentProgressDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentProgressDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentProgressDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
