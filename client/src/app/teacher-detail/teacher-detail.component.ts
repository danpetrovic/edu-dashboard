import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { ApiService } from '../api.service';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { MatRippleModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { ActivityAssessmentDetailComponent } from '../activity-assessment-detail/activity-assessment-detail.component';
import { ActivityAssessmentFormComponent } from '../activity-assessment-form/activity-assessment-form.component';
import { Subject, takeUntil } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-teacher-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule,
    MatListModule,
    MatRippleModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './teacher-detail.component.html',
  styleUrl: './teacher-detail.component.scss'
})
export class TeacherDetailComponent implements OnInit, OnDestroy {

  instance: any;
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private api: ApiService,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.instance = result['instance'];
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  getSubjectsTaughtDisplay(assessment: any): string {
    return assessment.subjects_taught.map((v: any) => v.name).join(', ');
  }

  del(): void {
    this.api.deleteTeacher(this.instance.id).subscribe(() => {
      this.snackbar.open('Teacher deleted.', undefined, {
        duration: 3000
      });
      this.router.navigate(['/teachers']);
    });
  }

  openActivityDetail(assessment: any): void {
    const dialogRef = this.dialog.open(ActivityAssessmentDetailComponent, {
      data: { instance: assessment, teacher: this.instance.id },
      autoFocus: false,
      minWidth: '400px'
    });
    dialogRef.componentInstance.edit.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(() => {
      dialogRef.afterClosed().pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.openActivityForm(assessment);
      });
      dialogRef.close();
    });
    dialogRef.componentInstance.del.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(() => {
      this.api.deleteActivityAssessment(assessment.id).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Activity deleted.', undefined, {
          duration: 3000
        });
        this.api.getActivityAssessmentList(this.instance.id).pipe(
          takeUntil(this.unsubscribe)
        ).subscribe(page => {
          this.instance.assessments = page.results;
          dialogRef.close();
        });
      });
    });
  }

  openActivityForm(assessment?: any): void {
    const dialogRef = this.dialog.open(ActivityAssessmentFormComponent, {
      data: { instance: assessment, teacher: this.instance.id },
      autoFocus: false,
      minWidth: '400px'
    });
    dialogRef.componentInstance.formSubmitted.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(action => {
      this.api.getActivityAssessmentList(this.instance.id).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(page => {
        this.instance.assessments = page.results;
        dialogRef.close();
      });
    });
  }
}
