import { Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { ApiService } from '../api.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-student-progress-list',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    JoinPipe,
  ],
  templateUrl: './student-progress-list.component.html',
  styleUrl: './student-progress-list.component.scss'
})
export class StudentProgressListComponent implements OnInit {

  teachers: any;
  subjects: any;
  studentProgress: any;
  subjectFilter = new FormControl('');
  teacherFilter = new FormControl('');

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.teachers = result['teacherListPage'].results;
      this.subjects = result['subjectListPage'].results;
      this.studentProgress = result['studentProgressListPage'].results;
    });
  }

  compareFilterObjects(o1: any, o2: any): boolean {
    return o1?.id === o2?.id;
  }

  runFilters(): void {
    const subject = this.subjectFilter.value || undefined;
    const teacher = this.teacherFilter.value || undefined;
    this.api.getStudentProgressList(subject, teacher).subscribe(page => {
      this.studentProgress = page.results;
    });
  }
}
