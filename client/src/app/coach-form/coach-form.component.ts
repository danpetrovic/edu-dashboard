import { Component, EventEmitter, Inject, OnDestroy, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../api.service';
import { Subject, takeUntil } from 'rxjs';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-coach-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './coach-form.component.html',
  styleUrl: './coach-form.component.scss'
})
export class CoachFormComponent implements OnInit, OnDestroy {

  @Output() formSubmitted = new EventEmitter();

  instance: any;
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    specialization: new FormControl('', Validators.required),
    years_of_experience: new FormControl(null, [Validators.required, Validators.min(0)])
  });
  newAvatarFile: File|null = null;
  avatarSrc: string|null = null;
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private dialogRef: MatDialogRef<CoachFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.instance = data.instance;
  }

  ngOnInit(): void {
    if (this.instance) {
      this.avatarSrc = this.instance.avatar;
      this.form.setValue({
        name: this.instance.name,
        specialization: this.instance.specialization,
        years_of_experience: this.instance.years_of_experience
      });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  chooseAvatarFile(): void {
    const input = document.createElement('input');
    input.type = 'file';
    input.addEventListener('change', () => {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.avatarSrc = reader.result as string;
        this.newAvatarFile = input.files![0];
      });
      reader.readAsDataURL(input.files![0]);
    });
    input.click();
  }

  removeAvatar(): void {
    this.avatarSrc = null;
    this.newAvatarFile = null;
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const formData = new FormData();
    formData.append('name', this.form.value.name!);
    formData.append('specialization', this.form.value.specialization!);
    formData.append('years_of_experience', this.form.value.years_of_experience!);
    if (this.newAvatarFile) {
      formData.append('avatar', this.newAvatarFile, this.newAvatarFile.name);
    } else if (!this.avatarSrc) {
      formData.append('avatar', '');
    }
    if (this.instance) {
      this.api.updateCoach(this.instance.id, formData).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Coach updated.', undefined, {
          duration: 3000
        });
        this.formSubmitted.emit();
      });
    } else {
      this.api.createCoach(formData).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Coach created.', undefined, {
          duration: 3000
        });
        this.formSubmitted.emit();
      });
    }
  }
}
