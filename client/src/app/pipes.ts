import { Pipe, PipeTransform } from "@angular/core";
import { environment } from "../environments/environment.development";



@Pipe({
  name: 'join',
  standalone: true
})
export class JoinPipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    return value.join(args[0]);
  }
}

@Pipe({
  name: 'mediaUrlCss',
  standalone: true
})
export class MediaUrlCssPipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    // Expects a leading slash on the value fragment.
    return `url('${environment.baseUrl}${value}')`;
  }
}