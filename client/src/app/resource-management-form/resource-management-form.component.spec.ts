import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceManagementFormComponent } from './resource-management-form.component';

describe('ResourceManagementFormComponent', () => {
  let component: ResourceManagementFormComponent;
  let fixture: ComponentFixture<ResourceManagementFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResourceManagementFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ResourceManagementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
