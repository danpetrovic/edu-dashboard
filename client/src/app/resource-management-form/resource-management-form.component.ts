import { Component, OnDestroy, OnInit } from '@angular/core';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { Subject, takeUntil } from 'rxjs';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-resource-management-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './resource-management-form.component.html',
  styleUrl: './resource-management-form.component.scss'
})
export class ResourceManagementFormComponent implements OnInit, OnDestroy {

  teachers: any;
  instance: any;
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    allocated_teachers: new FormControl([], Validators.required),
    utilization_rate: new FormControl(null, Validators.required)
  });
  unsubscribeSubject = new Subject<void>();
  unsubscribe = this.unsubscribeSubject.asObservable();

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.teachers = result['teacherListPage'].results;
      if (result['instance']) {
        this.instance = result['instance'];
        this.form.setValue({
          name: this.instance.name,
          allocated_teachers: this.instance.allocated_teachers,
          utilization_rate: this.instance.utilization_rate
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  compareSelectObjects(o1: any, o2: any): boolean {
    return o1?.id === o2?.id;
  }

  cancel(): void {
    this.router.navigate(['/resource-management']);
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const data = {
      name: this.form.value.name,
      allocated_teachers: (<any>this.form.value.allocated_teachers).map((v: any) => v.id),
      utilization_rate: this.form.value.utilization_rate
    };
    if (this.instance) {
      this.api.updateResourceManagement(this.instance.id, data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Resource updated.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/resource-management']);
      });
    } else {
      this.api.createResourceManagement(data).pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(() => {
        this.snackbar.open('Resource created.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/resource-management']);
      });
    }
  }
}
