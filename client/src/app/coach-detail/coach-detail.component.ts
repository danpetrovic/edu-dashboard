import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogTitle } from '@angular/material/dialog';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-coach-detail',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatSnackBarModule,
    JoinPipe
  ],
  templateUrl: './coach-detail.component.html',
  styleUrl: './coach-detail.component.scss'
})
export class CoachDetailComponent {

  @Output() del = new EventEmitter();
  @Output() edit = new EventEmitter();

  instance: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.instance = data.instance;
  }
}
