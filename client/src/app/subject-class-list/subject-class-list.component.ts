import { Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { JoinPipe } from '../pipes';
import { ApiService } from '../api.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-subject-class-list',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    MatButtonModule,
    JoinPipe,
  ],
  templateUrl: './subject-class-list.component.html',
  styleUrl: './subject-class-list.component.scss'
})
export class SubjectClassListComponent implements OnInit {

  classes: any;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      this.classes = result['subjectClassListPage'].results;
    });
  }
}
