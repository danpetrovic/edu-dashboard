import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectClassListComponent } from './subject-class-list.component';

describe('SubjectClassListComponent', () => {
  let component: SubjectClassListComponent;
  let fixture: ComponentFixture<SubjectClassListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SubjectClassListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SubjectClassListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
