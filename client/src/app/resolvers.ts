import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from "@angular/router";
import { ApiService } from "./api.service";


export const overviewResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getOverviewData();
}

export const subjectListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getSubjectList();
}

export const subjectResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getSubject(route.paramMap.get('subjectId')!);
}

export const teacherListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getTeacherList();
}

export const teacherResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getTeacher(route.paramMap.get('teacherId')!);
}

export const activityAssessmentListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getActivityAssessmentList();
}

export const activityAssessmentResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getActivityAssessment(route.paramMap.get('activityAssesmentId')!);
}

export const subjectClassListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getSubjectClassList();
}

export const subjectClassResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getSubjectClass(route.paramMap.get('subjectClassId')!);
}

export const studentProgressListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getStudentProgressList();
}

export const studentProgressResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getStudentProgress(route.paramMap.get('studentProgressId')!);
}

export const resourceManagementListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getResourceManagementList();
}

export const resourceManagementResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getResourceManagement(route.paramMap.get('resourceManagementId')!);
}

export const coachListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getCoachList();
}

export const coachResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getCoach(route.paramMap.get('coachId')!);
}

export const coachTeacherInteractionListResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getCoachTeacherInteractionList();
}

export const coachTeacherInteractionResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  api: ApiService = inject(ApiService)
) => {
  return api.getCoachTeacherInteraction(route.paramMap.get('coachTeacherInteractionId')!);
}