import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { JoinPipe } from '../pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { ApiService } from '../api.service';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, MomentDateModule } from '@angular/material-moment-adapter';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-teacher-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MomentDateModule,
    JoinPipe
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: ['YYYY-MM-DD']
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'MMM YYYY',
        }
      }
    },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
  ],
  templateUrl: './teacher-form.component.html',
  styleUrl: './teacher-form.component.scss'
})
export class TeacherFormComponent implements OnInit {

  instance: any;
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    date_of_birth: new FormControl(null, Validators.required),
    notes: new FormControl('')
  });
  newAvatarFile: File|null = null;
  avatarSrc: string|null = null;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(result => {
      if (result['instance']) {
        this.instance = result['instance'];
        this.avatarSrc = this.instance.avatar;
        this.form.setValue({
          name: this.instance.name,
          date_of_birth: this.instance.date_of_birth,
          notes: this.instance.notes
        });
      }
    });
  }

  chooseAvatarFile(): void {
    const input = document.createElement('input');
    input.type = 'file';
    input.addEventListener('change', () => {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.avatarSrc = reader.result as string;
        this.newAvatarFile = input.files![0];
      });
      reader.readAsDataURL(input.files![0]);
    });
    input.click();
  }

  removeAvatar(): void {
    this.avatarSrc = null;
    this.newAvatarFile = null;
  }

  cancel(): void {
    this.router.navigate(['/teachers'])
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (!this.form.valid) {
      return;
    }
    const dob = typeof this.form.value.date_of_birth === 'string' ? this.form.value.date_of_birth : (<any>this.form.value.date_of_birth).format('YYYY-MM-DD');
    const formData = new FormData();
    formData.append('name', this.form.value.name!);
    formData.append('date_of_birth', dob);
    formData.append('notes', this.form.value.notes!);
    if (this.newAvatarFile) {
      formData.append('avatar', this.newAvatarFile, this.newAvatarFile.name);
    } else if (!this.avatarSrc) {
      formData.append('avatar', '');
    }
    if (this.instance) {
      this.api.updateTeacher(this.instance.id, formData).subscribe(() => {
        this.snackbar.open('Teacher updated.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/teachers']);
      });
    } else {
      this.api.createTeacher(formData).subscribe(() => {
        this.snackbar.open('Teacher created.', undefined, {
          duration: 3000
        });
        this.router.navigate(['/teachers']);        
      });
    }
  }
}
