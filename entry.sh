#!/bin/bash

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install v20.9.0
npm install -g @angular/cli

cd /opt/client
npm install --include=dev
bash -c "ng serve --host 0.0.0.0 --port 4200" &

service postgresql start;
su postgres -c "psql -c \"CREATE ROLE dan WITH LOGIN SUPERUSER PASSWORD 'password'\"";
su postgres -c "createdb educational-dashboard";

cd /opt/server;
django-admin migrate;
django-admin populate;
django-admin runserver 0.0.0.0:8000;
